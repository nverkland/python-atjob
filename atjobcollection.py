#
# Copyright 2019, N. Verkland <nverkland@gmail.com>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3.0 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.
#
""" dict like AtJobCollection indexed on jobid """

import os
import collections
import subprocess as sp
from atjob import AtJob

ATCMD = "/usr/bin/at"

""" ------------------------
    NOTES BEFORE WE GET INTO THE GOODS
    Reminder: 12 digit number is simplist form of timespec
            (TIME_SPEC: CCYYMMDDhhmm)
    EG: at -m -t 201912041710 <enter>
      cat /tmp/crap <ctl-d>
    Reminder: when not using -t timespec is very broad
            including teatime, midnight, noon and others
    EG: at -m teatime tuesday <enter>
      cat /tmp/crap <ctl-d>
--------------------------- """

class AtJobCollection(collections.abc.MutableMapping):
    """ Class as close to mutablemapping as possible. Only
        thing I believe to be not compliant is __init__
    """

    # Minimum environment variables nessisary
    # when at jobs use "perl" or "python" or
    # "java" or "curl". Set as appropriate.
    # (see method _setenv)
    # -----
    # This will become more important in future
    # phases when we start to manipulate job
    # definitions directly.
    pths = ['/sites/utils/local/bin', '/sites/utils/bin', '/sites/utils/sbin',
            '/home/build/bin', '/home/build/sbin', '/usr/local/bin', '/usr/local/sbin',
            '/usr/bin', '/bin', '/usr/sbin', '/sbin']
    MINIMUM_ENV = {"SHELL":"/bin/bash", "LC_ALL":"C", "SHLVL":"1", "PATH":":".join(pths),
                   "JAVA_HOME":"/sites/jdk", "JAVA_VM":"/sites/jdk/bin/java",
                   "ROCACHE_HOME":"/app03/rocache_py", "HOME":"/app03/rocache_py"}

    def __init__(self):
        self.jobs = {}
        self.env = {}
        self._setenv()
        self._load()

    def _setenv(self, additional_vars=None):
        """ set environment variables but allow
            values set in shell to override
        """
        if not additional_vars:
            additional_vars = {}
        os.umask(0o22)
        self.env.update(AtJobCollection.MINIMUM_ENV)
        self.env.update(additional_vars)
        for key, value in self.env.items():
            value = os.getenv(key, value)
            os.environ[key] = value
            self.env[key] = value
        return self.env

    def _load(self):
        """ load at jobs from command line """
        proc = sp.Popen([ATCMD, '-l'], stdout=sp.PIPE, stderr=sp.PIPE)
        (out, err) = proc.communicate()
        if proc.returncode != 0:
            raise IOError(f"Error getting AT JOBS: {err}")
        lines = out.decode('utf-8').split("\n")
        for line in lines:
            if not line.strip() == '':
                j_obj = AtJob.from_jobstr(line)
                self.jobs[j_obj.jobid] = j_obj

    def __setitem__(self, key, value):
        """ put job-obj into jobs-dict """
        if not isinstance(value, AtJob):
            raise ValueError("cannot put a non-AtJob into AtJobCollection")
        j_obj = self.jobs.get(key, None)
        if j_obj:
            if not j_obj == value:
                # note: calling delete on a job object will remove it from the OS
                j_obj.delete()
        self.jobs[key] = value

    def __contains__(self, key):
        """ key found or not """
        return key in self.jobs.keys()

    def __getitem__(self, key):
        """ grab item at key or fail """
        return self.jobs[key]

    def __delitem__(self, key):
        """ delete item from dict (passively) """
        j = self.jobs.get(key, None)
        if j:
            j.delete()
        del self.jobs[key]

    def __iter__(self):
        """ iterate over job objects and their jobids """
        return self.jobs.__iter__()

    def __reversed__(self):
        keys = sorted(self.jobs.keys(), reverse=True)
        results = {key:self.jobs[key] for key in keys}
        return results

    def __len__(self):
        return len(self.jobs)

    def __len_hint__(self):
        return self.__len__()

    def __str__(self):
        """ create multiline string representation of collection """
        one_long_str = ''
        for jobj in self.jobs.values():
            one_long_str += str(jobj) + '\n'
        return one_long_str

    def __repr__(self):
        """ create multiline string representation of collection """
        return self.__str__()

    def keys(self):
        """ return list of jobids """
        return self.jobs.keys()

    def values(self):
        """ return list of job objects """
        return self.jobs.values()

    def items(self):
        """ iterate over the dict of jobs """
        for key, value in self.jobs.items():
            yield (key, value)

    def get(self, key, default=None):
        """ passive find key """
        return self.jobs.get(key, default)

    def clear(self):
        """ delete all jobs """
        for jobobj in self.jobs.values():
            jobobj.delete()
        keys = list(self.jobs.keys())
        for key in keys:
            del self.jobs[key]

    # skipping set default method until later if ever
    # def setdefault(self,value)

    def append(self, job):
        """ add job object to the jobs dict """
        if not isinstance(job, AtJob):
            raise ValueError("Cannot add non-AtJob to AtJobCollection")
        jid = job.jobid
        self.jobs[jid] = job

    def update(*args, **kwds):
        """ update job objects in this collection with the job objs provided

            There is a potential for a race condition here.  If col_1 gets
            created, then col_2 and col_2.update(col_1) is called it is
            possible for certain jobs to go missing.

            More interactivity with the underlying unix 'at' mechanism is
            required.
        """
        if not args:
            raise TypeError("descriptor 'update' of 'AtJobCollection' object "
                            "needs an argument")
        self, *args = args
        if len(args) > 1:
            raise TypeError(f"update expected at most 1 argument, got {len(args)}")
        if args:
            other = args[0]
            if isinstance(other, AtJobCollection):
                for key in other.keys():
                    self.jobs[key] = other[key]
            else:
                raise ValueError("Cannot update AtJobCollection with a non-AtJobCollection")
        for key, value in kwds.items():
            if isinstance(value, AtJob):
                self.jobs[key] = value
