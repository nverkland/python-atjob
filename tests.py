import atjob
from atjobcollection import AtJobCollection

if __name__ == '__main__':

    # tests on AtDateTime
    # -------------------
    user_date_strings = ['wednesday', 'next wednesday', 'now + 4 days', 'tomorrow',
                         'yesterday + 1 month','june 3','next june 3','2am next june 3',
                         '2am + 3 hours 2 minutes', 'july 14 2022', '1am july 14 2099',
                         'Wed Dec  4 17:10:00 2019','2019-12-13 15:00']
    for uds in user_date_strings:
        timeobj = atjob._parse_datetime(uds)
        print('')
        print(uds)
        print(timeobj.strftime("%Y-%m-%d %H:%M"))
    print("")

    # tests on AtJob
    # -------------------
    jtime = '1pm next tuesday'
    jcmds = ['cat /tmp/craptextfile','ls -lrt /tmp/*']
    print(f"creating job {jtime}")
    print(f"job will run: {jcmds}")
    my_at_job = atjob.AtJob(jcmds, jtime)
    jid = my_at_job.jobid
    print(f"TEST1: job {jid} created")
    print(f"string representation of job: {my_at_job}")
    my_at_job.delete()
    print(f"TEST2: job {jid} deleted")

    # TESTS ON AtJobCollection
    # -------------------

    # print jobs found on OS
    thing = AtJobCollection()
    print(thing)

    #  print jobs found on OS and the commands they will run
    thing = AtJobCollection()
    for jobid,jobobj in thing.jobs.items():
        print(jobobj)
        print(jobobj.list())
        print()

    # create job that cats out a file and add it to the collection
    cmds = ['cat /tmp/bark']
    my_at_job = atjob.AtJob(cmds, '1pm next tuesday')
    new_jobid = my_at_job.jobid
    print(f"created job {new_jobid}")
    thing.append(my_at_job)
    print(f"added job \"{new_jobid}\" to collection")

    # delete sample job created above (removes it from the OS also)
    del(thing[new_jobid])
    print(f"deleted job {new_jobid}")

    # create 2 jobs for messing with
    maj = atjob.AtJob(['ls -lrt /tmp/carp*', 'cat /tmp/crapcarpcoop'], '5pm next tuesday')
    thing.append(maj)
    maj = atjob.AtJob(['ls -lrt /tmp/carp*', 'cat /tmp/crapcarpcoop'], '1pm monday feb 3 2020')
    thing.append(maj)

    # test itterating on a collection
    for job in iter(thing): print(job)
    for k,v in thing.items(): print(f"{k}:{v}")

    # test comparing 2 jobs
    omaj = thing[maj.jobid]
    if omaj == maj: print("Compare success")
    else: print("Compare fail")

    # print commands a job will run
    print(maj.list())
    
    # WARNING IF YOU RUN THIS NEXT TEST IS WILL BE DESTRUCTIVE
    # TO EXISTING AT JOBS ON YOUR MACHINE
#    thing.clear()
#    print(f"{len(thing)} jobs left after clear")
