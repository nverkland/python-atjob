# python-atjob

This is a follow-on project to python-crontab. In this project we interact with cron's cousin; the Unix/Linux/BSD/Darwin command called 'at'. This project will be for creating, deleting, rescheduling, augmenting and controlling batch or 'at' jobs.


In this directory you'll find 3 class files and this readme.
 (atdatetime.py, atjob.py, atjobcollection.py, and readme)
Each class file has test cases coded into it. To excersize 
the tests you can execute each file "python3 <filename>" on
the command line of a MAC or LINUX based host.

The code for the test cases (in each file) start after the
line of code that includes: if __name__ == '__main__':

Below is information about the underlying unix command that 
we're interacting with in this set of python classes. Simmilar
information can be seen in the unix man page for 'at'.

NAME
       at "create, examine or delete jobs for later execution"

SYNOPSIS
       at [-f file] [-mMlbv] TIME                 #CREATE
       at [-f file] [-mMlbv] -t [[CC]YY]MMDDhhmm  #CREATE
       at -c job [job...]                         #CAT JOB(s)
       at [ -r ] job [job...]                     #REMOVE JOB(s)
       at [ -l ] job [job...]                     #LIST JOB(s)


DESCRIPTION
       at and batch read commands from standard input or a specified file
       which are to be executed at a later time.

       at      executes commands at a specified time.

       at -l   lists the user's pending jobs, unless the user is the supe-
               ruser; in that case, everybody's jobs are listed.  The format
               of the output lines (one for each job) is: Job number, date,
               hour, queue, and username.

       at -r   deletes jobs, identified by their job number.

       At allows fairly complex time specifications, extending the POSIX.2
       standard.   

       TIME: (defn: /usr/share/doc/at-3.1.10/timespec)
            HH:MM (YYYY-MM-DD)
            now + nn (minutes|hours|days|weeks)
            HH:MM tomorrow
            HH:MM today

       EXAMPLES:
            4pm  + 3 days
            10am Jul 31
            1am  tomorrow.

       Commands are read from standard input or the -f <file> option and executed.
       The superuser may use at in any case. For other users, permission 
       to use at is determined by the files /etc/at.allow and /etc/at.deny.

OPTIONS
       -f file Reads the job from file rather than standard input.
       -l      Is an alias for atq.
       -r      Is an alias for atrm.
       -v      Shows  the  time the job will be executed before reading the job.
       -c      cats the jobs listed on the command line to standard out-put.
       -t targ Submit the job to be run at the  time  specified  by  the
               targ option argument, which must have the same format as 
               specified for the touch(1) utility's  -t  time  option
               argument ([[CC]YY]MMDDhhmm).

SPECIAL = TODAY,TOMORROW,NOW,NEXT

/* 
 * Abbreviated version of the yacc grammar used by at(1).
 */

NOW = ['NOW']
AMPM = ['AM','PM']
SPECIAL_TIME_STR = ['NOON'.'MIDNIGHT','TEATIME']
DSTR= ['SUN','MON','TUE','WED','THU','FRI','SAT']
FDAY = ['TODAY','TOMORROW']
NEXT = ['NEXT']
TPERIOD = ['MINUTE','HOUR','DAY','WEEK','MONTH','YEAR']
MSTR = ['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC']
UTC = ['UTC']


WDAY_ABBR = '%a' # TUE
WDAY_LONG = '%A' # TUESDAY
WDAY_DECI = '%w' # 0..6
DOM_DECI = '%d'  # 01..31
MON_ABBR = '%b'  # JAN
MON_LONG = '%B'  # JANUARY
MON_DECI = '%m'  # 01..12
YR_2DIG = '%y'   # 00..99
YR_4DIG = '%Y'   # 1974
HR_24 = '%H'     # 00 - 23
HR_12 = '%I'     # 01 - 12
AM_PM = '%P'     # AM or PM
MIN_DECI = '%M'  # 00..59
DOY_DECI = '%j'  # 00..366
DOTDATE = ('+YR_4DIG+'|'+YR_2DIG+')'+'.'+MON_DECI+'.'+DOM_DECI
DOTDATE = YR_4DIG '|' YR_2DIG '.' MON_DECI '.' DOM_DECI

# 1974.02.14, 74.05.21, 1974|11|29, 74|01|22
date_formats.append(YR_4DIG+'.'+MON_DECI+'.'+DOM_DECI)
date_formats.append(YR_2DIG+'.'+MON_DECI+'.'+DOM_DECI)
date_formats.append(YR_4DIG+'|'+MON_DECI+'|'+DOM_DECI)
date_formats.append(YR_2DIG+'|'+MON_DECI+'|'+DOM_DECI)

# JANUARY 12 -or- JAN 19 
date_formats.append(MON_LONG+' '+DOM_DECI')
date_formats.append(MON_ABBR+' '+DOM_DECI')

# JANUARY 12 88 -or- JANUARY 12 1988 -or- JAN 12 88 -or- JAN 12 1988
# JANUARY 12,88 -or- JANUARY 12,1988 -or- JAN 12,88 -or- JAN 12,1988
date_formats.append(MON_LONG+' '+DOM_DECI'+' '+YR_2DIG)
date_formats.append(MON_LONG+' '+DOM_DECI'+' '+YR_4DIG)
date_formats.append(MON_ABBR+' '+DOM_DECI'+' '+YR_2DIG)
date_formats.append(MON_ABBR+' '+DOM_DECI'+' '+YR_4DIG)
date_formats.append(MON_LONG+' '+DOM_DECI'+','+YR_2DIG)
date_formats.append(MON_LONG+' '+DOM_DECI'+','+YR_4DIG)
date_formats.append(MON_ABBR+' '+DOM_DECI'+','+YR_2DIG)
date_formats.append(MON_ABBR+' '+DOM_DECI'+','+YR_4DIG)

time_formats.append(HR_24+':'+MIN_DECI)
time_formats.append(HR_12+':'+MIN_DECI)
time_formats.append(HR_24)
time_formats.append(HR_12)

date            : month_name day_number
                | month_name day_number year_number
                | month_name day_number ',' year_number
                | day_of_week
                | TODAY
                | TOMORROW
                | HYPHENDATE
                | DOTTEDDATE
                | day_number month_name
                | day_number month_name year_number
                | month_number '/' day_number '/' year_number
                | concatenated_date
                | NEXT inc_dec_period           
                | NEXT day_of_week

      YYYYMMDD
06060_12121212

%token  <charval> DOTTEDDATE
%token  <charval> HYPHENDATE
%token  <charval> HOURMIN
%token  <charval> INT1DIGIT
%token  <charval> INT2DIGIT
%token  <charval> INT4DIGIT
%token  <charval> INT5_8DIGIT
%token  <charval> INT


%type <charval> concatenated_date
%type <charval> hr24clock_hr_min
%type <charval> int1_2digit
%type <charval> int2_or_4digit
%type <charval> integer
%type <intval> inc_dec_period
%type <intval> inc_dec_number
%type <intval> day_of_week

%start timespec
%%
timespec        : spec_base
                | spec_base inc_or_dec
                ;

spec_base       : date
                | time
                | time date
                | NOW
                ;

time            : time_base
                | time_base timezone_name
                ;

time_base       : hr24clock_hr_min
                | time_hour am_pm
                | time_hour_min
                | time_hour_min am_pm
                | NOON
                | MIDNIGHT
                | TEATIME
                ;


hr24clock_hr_min: INT4DIGIT
                ;

time_hour       : int1_2digit
                ;

time_hour_min   : HOURMIN
                ;

am_pm           : AM
                | PM
                ;

timezone_name   : UTC
                ;

date            : month_name day_number
                | month_name day_number year_number
                | month_name day_number ',' year_number
                | day_of_week
                | TODAY
                | TOMORROW
                | HYPHENDATE
                | DOTTEDDATE
                | day_number month_name
                | day_number month_name year_number
                | month_number '/' day_number '/' year_number
                | concatenated_date
                | NEXT inc_dec_period           
                | NEXT day_of_week
                ;


concatenated_date: INT5_8DIGIT
                ;

month_name      : JAN | FEB | MAR | APR | MAY | JUN
                | JUL | AUG | SEP | OCT | NOV | DEC
                ;

month_number    : int1_2digit
                ;

day_number      : int1_2digit
                ;

year_number     : int2_or_4digit
                ;

day_of_week     : SUN | MON | TUE | WED | THU | FRI | SAT
                ;

inc_or_dec      : increment | decrement
                ;

increment       : '+' inc_dec_number inc_dec_period
                ;

decrement       : '-' inc_dec_number inc_dec_period
                ;

inc_dec_number  : integer
                ;

inc_dec_period  : MINUTE | HOUR | DAY | WEEK | MONTH | YEAR
                ;

int1_2digit     : INT1DIGIT | INT2DIGIT
                ;

int2_or_4digit  : INT2DIGIT | INT4DIGIT
                ;

integer         : INT | INT1DIGIT | INT2DIGIT | INT4DIGIT | INT5_8DIGIT
                ;

%%
