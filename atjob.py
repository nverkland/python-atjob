#
# Copyright 2019, N. Verkland <nverkland@gmail.com>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3.0 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.
#
""" AtJob is an object representing a schedualed job registered with the OS.
    Manipulating an AtJob will directly change the jobs definition in the OS.

    Also methods for reading datetimes from timespec of unix command 'at'

    Read each line from "at -l" and turn it into an AtJob object.

    Known defect (somewhat intentional) if the OS has an AT JOB that
    is schedualed in the past, when this set of classes reads that job
    it will display the job as being in the future. The intent here
    is that when a user attempts to create a job, jobs attempted for
    the past will be schedualed for the next convienent future time
    on the users behalf.
"""

import sys
import re
import pwd
import os
import subprocess as sp
import datetime
import calendar
import dateparser
import dateutil.relativedelta as dtrel

ATCMD = "/usr/bin/at"

class FormatError(ValueError):
    """ Format of the AtJob string is invalid """
    pass

M_NAMES = {calendar.month_name[i+1].lower():i+1 for i in range(12)}
M_ABBRS = {calendar.month_abbr[i+1].lower():i+1 for i in range(12)}

D_NAMES = {calendar.day_name[i].lower():i for i in range(7)}
D_ABBRS = {calendar.day_abbr[i].lower():i for i in range(7)}

def _parse_dt_kwargs(kwstring):
    """ method to handle strings such as:
     '+ 3 weeks 2 days'
     '+ 1 week'
     '+ 1 day 2 hours'
     '+ june 3'
     'next tuesday'
     'next month'
     'next june'
     'next june 3'
     '+ 2 hours 30 minutes'
    breaks these into k,v pairs
    """
    kwargs = {}
    fields = kwstring.split(' ')
    fields = [x.strip() for x in fields]
    word = None
    count = 0
    if fields[0] in ('+', 'next'):
        fields.pop(0)
    if len(fields) > 0:
        word = fields[0]
        fields.pop(0)
    if len(fields) > 0:
        count = fields[0]
        fields.pop(0)
    if word.isdigit() and count != 0:
        # reverse word and count variables
        # for situation like "june 3" instead of "3 months"
        ctmp = count
        count = int(word)
        word = ctmp
    if len(fields) > 0:
        # if there are more k,v pairs then recurse.
        kwargs = _parse_dt_kwargs(" ".join(fields))
    if word in ['week', 'day', 'hour', 'month']:
        # handle special case
        word += 's'
    if word:
        if word in D_NAMES:
            kwargs['weekday'] = D_NAMES[word]
        elif word in D_ABBRS:
            kwargs['weekday'] = D_ABBRS[word]
        elif word in M_NAMES:
            kwargs['month'] = M_NAMES[word]
            if count != 0:
                kwargs['day'] = int(count)
        elif word in M_ABBRS:
            kwargs['month'] = M_ABBRS[word]
            if count != 0:
                kwargs['day'] = int(count)
        else:
            kwargs[word] = int(count)
    return kwargs

def _parse_datetime(usr_datestr):
    """ split user date string into an inital date and
        'added time' (math_datestr) then calculate a
        datetime from those.
    """
    math_datestr = None
    return_date = usr_datestr.lower().strip()
    for token in [' + ', 'next ']:
        if token in usr_datestr:
            pos = usr_datestr.find(token)
            return_date = usr_datestr[:pos].strip()
            math_datestr = usr_datestr[pos:].strip()
            break

    # get an inital date out of the
    # first part of the usr_datestr
    now = datetime.datetime.now()
    if return_date in ('now', ''):
        return_date = now
    else:
        return_date = dateparser.parse(return_date)

    # add on any 'added time' found in the
    # usr_datestr (anything after "+" or "next")
    if math_datestr:
        kwargs = _parse_dt_kwargs(math_datestr)
        return_date = return_date + dtrel.relativedelta(**kwargs)

    # adjust date to ensure it is in the future
    if return_date != now and (return_date - now).days < 0:
        t_date = return_date + dtrel.relativedelta(days=1)
        if (t_date - now).days < 0:
            t_date = return_date + dtrel.relativedelta(weeks=1)
            if (t_date - now).days < 0:
                t_date = return_date + dtrel.relativedelta(months=1)
                if (t_date - now).days < 0:
                    t_date = return_date + dtrel.relativedelta(years=1)
        return_date = t_date

    # return the calculated date
    return return_date

class AtJob(object):
    """ AtJob is an object representing on schedualed job that is registered with the OS.
        Manipulating an AtJob will directly change the jobs definition in the OS.
    """

    def __init__(self, cmds, at_timespec, queue='A', user=None, jobid=-1):
        """ if jobid is not given, job will be submitted (immediately) to the OS
            as a schedualed event. If JobID is given, this class assumes the jobid
            is already registered with the os and only modifications will interact
            with OS
        """
        self.queue = queue
        self.when = _parse_datetime(at_timespec)
        if not user:
            user = pwd.getpwuid(os.getuid())[0]
        self.user = user
        self.commandlines = [c.strip() for c in cmds]
        if jobid == -1:
            # write job to the OS
            cargs = [ATCMD, '-t', self.when.strftime("%Y%m%d%H%M")]
            inbytes = "\n".join(self.commandlines).encode('utf-8')
            proc = sp.Popen(cargs, stdin=sp.PIPE, stdout=sp.PIPE, stderr=sp.PIPE)
            (out, err) = proc.communicate(input=inbytes)
            if proc.returncode == 0:
                lines = err.decode('utf-8').split("\n")
                (job, jobid, therest) = lines[0].split(' ', 2)
            else:
                raise IOError(f"Cannot create at job: {err}")
        self.jobid = jobid

    @staticmethod
    def read_darwin(jid):
        """ given a jobid, get the definition of job from operating system """
        proc = sp.Popen([ATCMD, '-c', jid], stdout=sp.PIPE, stderr=sp.PIPE)
        (out, err) = proc.communicate()
        if proc.returncode != 0:
            cuser = pwd.getpwuid(os.getuid())[0]
            raise IOError(f"Error getting AT JOB {jid} for {cuser}: {err}")
        clines = out.decode('utf-8').split("\n")
        cmds = []
        for cmdline in clines:
            if re.match(cmdline, '^#'):
                continue
            if re.match(cmdline, '^[0-9A-Za-z]*='):
                continue
            cmds.append(cmdline)
        return cmds

    @staticmethod
    def read_linux(jid):
        """ given a jobid, get the definition of job from operating system """
        proc = sp.Popen([ATCMD, '-c', jid], stdout=sp.PIPE, stderr=sp.PIPE)
        (out, err) = proc.communicate()
        if proc.returncode != 0:
            cuser = pwd.getpwuid(os.getuid())[0]
            raise IOError(f"Error getting AT JOB {jid} for {cuser}: {err}")
        clines = out.decode('utf-8').split("\n")
        cmds = []
        # Look for marker in the job file twice. When
        # second instance of marker is found, you're
        # done reading the commands the job will run.
        mfound = False
        for cmdline in clines:
            if mfound:
                cmds.append(cmdline)
            if cmdline.find('marcinDELIMITER') >= 0:
                if mfound:
                    cmds.pop()
                    break
                mfound = True
        return cmds

    @staticmethod
    def from_jobstr(at_jobstr):
        """ input one text line from 'at -l'.
            output AtJob python object
            method reads details of job from 'at -c <jid>'.
        """
        job = None
        plat = str(sys.platform).lower()
        if plat.startswith('darwin'):
            (jobid, datestr) = at_jobstr.split(' ', 1)
            cmds = AtJob.read_darwin(jobid)
            job = AtJob(cmds, datestr, jobid=jobid)
        elif plat.startswith('linux'):
            if not len(at_jobstr.split()) == 5:
                raise FormatError(f"Garbled AT JOB: {at_jobstr}")
            (jobid, date, time, queue, user) = at_jobstr.split()
            cmds = AtJob.read_linux(jobid)
            job = AtJob(cmds, date+' '+time, queue, user, jobid)
        else:
             # 'freebsd', 'solaris', 'aix', 'netbsd', 'openbsd'
            raise ValueError(f"{plat} code not implemented")
        return job

    def __eq__(self, other):
        """ instance equality based on jobid """
        if not isinstance(other, AtJob):
            return False
        return self.jobid == other.jobid

    def delete(self):
        """ remove job from OS """
        cargs = [ATCMD, '-d', self.jobid]
        proc = sp.Popen(cargs, stdout=sp.PIPE, stderr=sp.PIPE)
        (out, err) = proc.communicate()
        if proc.returncode != 0:
            raise IOError(f"Delete job {self.jobid} failed: {err}")

    def list(self):
        """ print commands that this job runs """
        return "\n".join(self.commandlines)

    def __str__(self):
        """ give same information at 'at -l' for this job """
        user_friendly_dt = self.when.strftime("%Y-%m-%d %H:%M")
        return f"{self.jobid} {user_friendly_dt} {self.queue} {self.user}"
